# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.21)
# Database: dev_mnode
# Generation Time: 2018-04-23 13:07:54 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table mn_master
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mn_master`;

CREATE TABLE `mn_master` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '',
  `ticker` varchar(30) DEFAULT '',
  `price` decimal(17,8) DEFAULT '0.00000000',
  `volume` decimal(17,2) DEFAULT '0.00',
  `marketcap` decimal(17,2) DEFAULT '0.00',
  `needed` decimal(17,8) DEFAULT NULL,
  `num_of_node` bigint(255) unsigned DEFAULT '0',
  `active` tinyint(2) DEFAULT '0',
  `reward_frequency` bigint(20) unsigned DEFAULT '0',
  `mn_min` decimal(17,8) DEFAULT '0.00000000',
  `mn_max` decimal(17,8) DEFAULT '0.00000000',
  `increment` decimal(17,8) DEFAULT '0.00000000',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT '0',
  `updated_by` bigint(20) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mn_master` WRITE;
/*!40000 ALTER TABLE `mn_master` DISABLE KEYS */;

INSERT INTO `mn_master` (`id`, `name`, `ticker`, `price`, `volume`, `marketcap`, `needed`, `num_of_node`, `active`, `reward_frequency`, `mn_min`, `mn_max`, `increment`, `created_at`, `updated_at`, `created_by`, `updated_by`)
VALUES
	(1,'ACOIN','Ticker',0.03000000,56700.00,456700.00,10000.00000000,23,1,3600,0.00000000,0.00000000,0.00000000,'2018-04-19 17:00:00','2018-04-19 17:00:00',1,1);

/*!40000 ALTER TABLE `mn_master` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mn_payout
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mn_payout`;

CREATE TABLE `mn_payout` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mn_trx_id` bigint(20) unsigned DEFAULT '0',
  `user_id` bigint(20) unsigned DEFAULT '0',
  `user_payout_amount` decimal(17,8) DEFAULT NULL,
  `payout_time` timestamp NULL DEFAULT NULL,
  `to_wallet_id` bigint(20) unsigned DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT '0',
  `updated_by` bigint(20) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mn_payout` WRITE;
/*!40000 ALTER TABLE `mn_payout` DISABLE KEYS */;

INSERT INTO `mn_payout` (`id`, `mn_trx_id`, `user_id`, `user_payout_amount`, `payout_time`, `to_wallet_id`, `created_at`, `updated_at`, `created_by`, `updated_by`)
VALUES
	(1,1,1,1000.00000000,'2018-04-03 20:49:00',1,'2018-04-03 20:49:00','2018-04-03 20:49:00',1,1),
	(2,1,1,1000.00000000,'2018-04-03 20:49:00',1,'2018-04-03 20:49:00','2018-04-03 20:49:00',1,1);

/*!40000 ALTER TABLE `mn_payout` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mn_trx
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mn_trx`;

CREATE TABLE `mn_trx` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mn_name` varchar(200) DEFAULT '',
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `participant` int(11) DEFAULT '0',
  `amount_hold` decimal(17,8) unsigned DEFAULT '0.00000000',
  `amount_need` decimal(17,8) DEFAULT '0.00000000',
  `mn_status` tinyint(4) DEFAULT '0',
  `last_payout_time` timestamp NULL DEFAULT NULL,
  `payout_count` int(10) unsigned DEFAULT NULL,
  `last_payout_amount` decimal(17,8) DEFAULT '0.00000000',
  `total_payout_amount` decimal(17,8) DEFAULT '0.00000000',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT '0',
  `updated_by` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mn_trx` WRITE;
/*!40000 ALTER TABLE `mn_trx` DISABLE KEYS */;

INSERT INTO `mn_trx` (`id`, `mn_name`, `start_date`, `end_date`, `participant`, `amount_hold`, `amount_need`, `mn_status`, `last_payout_time`, `payout_count`, `last_payout_amount`, `total_payout_amount`, `created_at`, `updated_at`, `created_by`, `updated_by`)
VALUES
	(1,'ACOIN','2018-04-03 20:49:00','2018-04-03 20:49:00',370,1000.00000000,1000.00000000,1,'2018-04-03 20:49:00',NULL,1000.00000000,1000.00000000,'2018-04-03 20:49:00','2018-04-03 20:49:00',1,1);

/*!40000 ALTER TABLE `mn_trx` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mn_trx_dtl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mn_trx_dtl`;

CREATE TABLE `mn_trx_dtl` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mn_trx_id` bigint(20) unsigned DEFAULT '0',
  `amount_stake` decimal(17,8) DEFAULT '0.00000000',
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `user_last_payout_time` timestamp NULL DEFAULT NULL,
  `user_last_payout_amount` decimal(17,8) DEFAULT '0.00000000',
  `user_total_payout_amount` decimal(17,8) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `updated_by` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mn_trx_dtl` WRITE;
/*!40000 ALTER TABLE `mn_trx_dtl` DISABLE KEYS */;

INSERT INTO `mn_trx_dtl` (`id`, `mn_trx_id`, `amount_stake`, `user_id`, `user_last_payout_time`, `user_last_payout_amount`, `user_total_payout_amount`, `created_at`, `updated_at`, `created_by`, `updated_by`)
VALUES
	(1,1,1000.00000000,1,'2018-04-03 20:49:00',1000.00000000,1000.00000000,'2018-04-03 20:49:00','2018-04-03 20:49:00',1,1),
	(2,1,1000.00000000,1,'2018-04-03 20:49:00',1000.00000000,1000.00000000,'2018-04-03 20:49:00','2018-04-03 20:49:00',1,1);

/*!40000 ALTER TABLE `mn_trx_dtl` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_transfer_in
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_transfer_in`;

CREATE TABLE `user_transfer_in` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mn_trx_id` bigint(20) unsigned DEFAULT '0',
  `amount` decimal(17,8) DEFAULT NULL,
  `trx_hash` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `updated_by` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table user_wallet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_wallet`;

CREATE TABLE `user_wallet` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT '0',
  `ticker` varchar(30) DEFAULT '',
  `address` varchar(250) DEFAULT '',
  `amount_total` decimal(17,8) DEFAULT '0.00000000',
  `amount_available` decimal(17,8) DEFAULT '0.00000000',
  `amount_lock` decimal(17,8) DEFAULT '0.00000000',
  `amount_mn` decimal(17,8) DEFAULT '0.00000000',
  `active` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT '0',
  `updated_by` bigint(20) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user_wallet` WRITE;
/*!40000 ALTER TABLE `user_wallet` DISABLE KEYS */;

INSERT INTO `user_wallet` (`id`, `user_id`, `ticker`, `address`, `amount_total`, `amount_available`, `amount_lock`, `amount_mn`, `active`, `created_at`, `updated_at`, `created_by`, `updated_by`)
VALUES
	(1,1,'ACO','0xfjladkjfl33kljlksABCd',300.00000000,100.00000000,50.00000000,150.00000000,1,'2018-04-18 10:21:45','2018-04-18 10:21:51',1,1);

/*!40000 ALTER TABLE `user_wallet` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table usermaster
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usermaster`;

CREATE TABLE `usermaster` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `email` varchar(200) DEFAULT '',
  `password` varchar(250) DEFAULT '',
  `remember_token` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `active` tinyint(4) DEFAULT '0',
  `activation_code` varchar(255) DEFAULT '',
  `reset_token` varchar(255) DEFAULT '',
  `token_expire` timestamp NULL DEFAULT NULL,
  `enable2fa` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT '0',
  `updated_by` bigint(20) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `usermaster` WRITE;
/*!40000 ALTER TABLE `usermaster` DISABLE KEYS */;

INSERT INTO `usermaster` (`id`, `username`, `email`, `password`, `remember_token`, `dob`, `active`, `activation_code`, `reset_token`, `token_expire`, `enable2fa`, `created_at`, `updated_at`, `created_by`, `updated_by`)
VALUES
	(1,'admin','admin@mail.com','$2y$10$rWeDU93JzjL8mk/zCmdo7u9Hm9uGaZxkz8BHIuMdT0VpsvRSxOYm6','WRLpeCRf1dZBgvl0tj3goqF8KWWKzdvAeHXVVjmhSdf3zHhLJxaxvGuLhEhz','2018-04-18',1,'','',NULL,0,'2018-04-18 10:18:27','2018-04-18 10:18:33',1,1),
	(2,'edi124','edy.senjaya@indosystem.com','$2y$10$wWAKY/SXulQoLv.ylTkRluvzPkIyyouohYjtn0oLRCFRxA4y9cZMy','NkY3Ni0XRyHZRMS2FumSQmLy40WitjCdvXGyw4b4nRuzzc3NLy9DcZ1GFXjJ',NULL,0,'','',NULL,0,'2018-04-20 09:23:01','2018-04-20 09:23:01',0,0),
	(3,'Kiddie','kiddie@mail.com','$2y$10$fusL/hCTjD0/y2LEyX6XIuk85vbtw/6WgaFy8DgMEWphtPFt7ssKO','4TpSKlGsfWmrAVoOrWTkHClVASIJ3iKWIgcbKlCwKexOLAwbtpSYnDcGhoDa',NULL,0,'','',NULL,0,'2018-04-21 20:34:52','2018-04-21 20:34:52',0,0);

/*!40000 ALTER TABLE `usermaster` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
