<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\NodesServices;

class NodesController extends Controller
{
    
    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * Display Master node index
     *
     * @return void
     */
    public function index()
    {
        return view('pages.nodes.index');
    }

    /**
     * Show Master Node Detail
     *
     * @param [type] $id
     * @return void
     */
    public function show($id)
    {
        $nodesServices = new NodesServices('mn_master');
        $data = $nodesServices->getShow($id);
        return view('pages.nodes.show', compact('data'));
    }

    /**
     * JSON DataTable Method 
     *
     * @return JSON
     */
    public function dataTable()
    {
        $nodesServices = new NodesServices('mn_master');
        return $nodesServices->getTable();
    }
}
