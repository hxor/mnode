<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\BalanceServices;

class BalancesController extends Controller
{
    public function index()
    {
        return view('pages.balances.index');
    }

    public function show($id)
    {
        return view('pages.balances.show');
    }

    public function dataTable()
    {
        $balanceServices = new BalanceServices('user_wallet');
        return $balanceServices->getTable();
    }
}
