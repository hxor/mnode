<?php

namespace App\Services;

use DB;
use DataTables;

class GlobalServices
{
    public function dataTable($table)
    {
        $data = DB::table($table);
        return DataTables::of($data);
    }
}
