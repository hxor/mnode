<?php

namespace App\Services;

use App\Services\GlobalServices;

class BalanceServices extends GlobalServices
{
    public $table;

    public function __construct($table)
    {
        $this->table = $table;
    }

    public function getTable(){
        return $this->dataTable($this->table)
            ->addColumn('amount_available', function ($data) {
                return number_format($data->amount_available);
            })
            ->addColumn('amount_lock', function ($data) {
                return number_format($data->amount_lock);
            })
            ->addColumn('amount_mn', function ($data) {
                return number_format($data->amount_mn);
            })
            ->addColumn('detail', function ($data) {
                return '<a href="' . route('balances.show', $data->id) . '">-></a>';
            })
            ->rawColumns(['detail'])
            ->make(true);
    }
}
