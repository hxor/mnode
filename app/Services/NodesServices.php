<?php

namespace App\Services;

use DB;
use App\Services\GlobalServices;

class NodesServices extends GlobalServices
{
    public $table;

    public function __construct($table)
    {
        $this->table = $table;
    }

    public function getShow($id)
    {
        $node = DB::table($this->table)->where('id', $id)->first();
        $rewardFrequency = $this->secondsToTime($node->reward_frequency);

        return $data = [
            'node' => $node,
            'rewardFrequency' => $rewardFrequency
        ];
    }

    public function getTable(){
        return $this->dataTable($this->table)
            ->addColumn('name', function ($nodes) {
                return '<a href="' . route('nodes.show', $nodes->id) . '">' . $nodes->name . '</a>';
            })
            ->addColumn('price', function ($nodes) {
                return '$' . number_format($nodes->price, 2);
            })
            ->addColumn('volume', function ($nodes) {
                return '$' . number_format($nodes->volume);
            })
            ->addColumn('marketcap', function ($nodes) {
                return '$' . number_format($nodes->marketcap);
            })
            ->addColumn('needed', function ($nodes) {
                return number_format($nodes->needed);
            })
            ->addColumn('worth', function ($nodes) {
                return '$300';
            })
            ->rawColumns(['name'])
            ->make(true);
    }

    /**
     * Convert Seconds to Time
     *
     * @param [type] $seconds
     * @return string
     */
    protected function secondsToTime($seconds) {
        $dtF = new \DateTime("@0");
        $dtT = new \DateTime("@$seconds");
        return $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');
    }
}
