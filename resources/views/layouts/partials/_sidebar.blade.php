<aside id="leftsidebar" class="sidebar">
    <ul class="nav nav-tabs">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#dashboard"><i class="zmdi zmdi-home m-r-5"></i>Home</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane stretchRight active" id="dashboard">
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN</li>
                    <li> <a href="{{ route('home') }}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-apps"></i><span>Nodes</span> </a>
                        <ul class="ml-menu">
                            <li><a href="{{ route('nodes.index') }}">Join New Nodes</a></li>
                            <li><a href="{{ route('my.nodes.index') }}">My Nodes</a></li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-apps"></i><span>Balance</span> </a>
                        <ul class="ml-menu">
                            <li><a href="{{ route('balances.index') }}">My Balance</a></li>
                        </ul>
                    </li>
                    <li class="header">EXTRA</li>
                    <li> <a href="setting.html"><i class="zmdi zmdi-settings"></i><span>Setting</span></a></li>
                    <li> 
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="zmdi zmdi-power"></i>
                            <span>Logout</span>
                        </a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </ul>
            </div>
        </div>
    </div>
</aside>