@extends('layouts.app')

@push('styles')
    <style type="text/css">    
        .col-centered {
            float: none;
            margin: 0 auto;
        }
    </style>
@endpush 

@section('content')
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>My Masternode
                <small>Welcome to Nodemaster</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Nodemaster</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                    <li class="breadcrumb-item active">My Masternode</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body table-responsive">
                        <div class="col-lg-4 col-centered">
                            <h3><strong>MN0001</strong> Masternode </h3>
                        </div>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="width: 18%;">Active Join Date</td>
                                    <td style="width: 1%";>:</td>
                                    <td>2018-04-03 20:49</td>
                                </tr>
                                <tr>
                                    <td>No of Contributor</td>
                                    <td>:</td>
                                    <td>370</td>
                                </tr>
                                <tr>
                                    <td>Your Coin</td>
                                    <td>:</td>
                                    <td>100 ACOIN (1.41 % 10,000 ACOIN)</td>
                                </tr>
                                <tr>
                                    <td>Total Coin in Masternode</td>
                                    <td>:</td>
                                    <td>10,000</td>
                                </tr>
                                <tr>
                                    <td>Total Paid to You</td>
                                    <td>:</td>
                                    <td>3 ACOIN</td>
                                </tr>
                                <tr>
                                    <td>Last Payout</td>
                                    <td>:</td>
                                    <td>2018-04-07 20:59</td>
                                </tr>
                                <tr>
                                    <td>Last Payout to You</td>
                                    <td>:</td>
                                    <td>0.05 ACOIN</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>:</td>
                                    <td>Active</td>
                                </tr>                           
                            </tbody>
                        </table>

                        <br>
                        <h6>Withdraw from This Masternode</h6>
                        <p>You can choose to withdraw from this masternode, but you have to wait for 3x24 Hours for other contributor to join</p>
                        <p>A fee of 0.1 A Coin will be deducted from your withdrawal amount if you choose to withdraw.</p>
                        <p>Click below if you are sure you want to withdraw fro this Masternode</p>
                        <div class="col-lg-4 col-centered">
                            <div class="form-group">
                                <button class="btn btn-outline-secondary btn-round" type="button" id="btn-withdraw">Withdraw from this Masternode</button>
                            </div>
                        </div>
                       <div class="col-lg-6 col-centered " style="display: none;" id="withdraw-proccess">
                            <form>
                                <div class="form-group">
                                    <input type="text" name="address" value="0xABCDE1134DDFF3424099BDF" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-info btn-round" type="submit"  style="margin-left: 164px;">Process Withdrawal</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function () {
            $("withdraw-proccess").hide();
            $("#btn-withdraw").click(function(){
                $("#withdraw-proccess").toggle();
            });
        })
    </script>
@endpush