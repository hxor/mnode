@extends('layouts.app')

@push('styles')
<!-- JQuery DataTable Css -->
<style type="text/css">
    .col-centered {
        float: none;
        margin: 0 auto;
    }
</style>
@endpush

@section('content')
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Home
                    <small>Welcome to Nodemaster</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Nodemaster</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body table-responsive">
                        <div class="col-lg-4 col-centered">
                            <h3><strong>My</strong> Masternode </h3>
                        </div>
                        <table class="table">
                            <thead class="bg-info text-white">
                                <tr>
                                    <th>Name</th>
                                    <th>MasternodeID</th>
                                    <th>Amount Reserved</th>
                                    <th>Last Payout</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">ACOIN</th>
                                    <td>MN0001</td>
                                    <td>100</td>
                                    <td>2018-04-03</td>
                                    <td><a href="{{ route('my.nodes.show') }}">-></a></td>
                                </tr>
                                <tr>
                                    <th scope="row">BCOIN</th>
                                    <td>MN0002</td>
                                    <td>30</td>
                                    <td>2018-04-05</td>
                                    <td><a href="{{ route('my.nodes.show') }}">-></a></td>
                                </tr>
                                <tr>
                                    <th scope="row">ACOIN</th>
                                    <td>MN0015</td>
                                    <td>1500</td>
                                    <td>2018-04-15</td>
                                    <td><a href="{{ route('my.nodes.show') }}">-></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection