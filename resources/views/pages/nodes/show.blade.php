@extends('layouts.app')

@push('styles')
    <style type="text/css">
        .tab {
            display: inline-block;
            margin-left: 40px;
        }
    
        .col-centered {
            float: none;
            margin: 0 auto;
        }
    </style>
@endpush 

@section('content')
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{{ $data['node']->name }} Masternode
                <small>Welcome to Nodemaster</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Nodemaster</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                    <li class="breadcrumb-item active">{{ $data['node']->name }} Masternode</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body table-responsive">
                        <div class="col-lg-4 col-centered">
                            <h3><strong>{{ $data['node']->name }}</strong> Masternode </h3>
                        </div>
                        <h6>Info</h6>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="width: 18%;">Price</td>
                                    <td style="width: 1%";>:</td>
                                    <td>$ {{ number_format($data['node']->price, 2) }}</td>
                                </tr>
                                <tr>
                                    <td>Reward Frequency</td>
                                    <td>:</td>
                                    <td>{{ $data['rewardFrequency'] }}</td>
                                </tr>
                                <tr>
                                    <td>Coin Required</td>
                                    <td>:</td>
                                    <td>{{ number_format($data['node']->needed) }}</td>
                                </tr>
                                <tr>
                                    <td>Volume</td>
                                    <td>:</td>
                                    <td>${{ number_format($data['node']->volume) }}</td>
                                </tr>
                                <tr>
                                    <td>Marketcap</td>
                                    <td>:</td>
                                    <td>${{ number_format($data['node']->marketcap) }}</td>
                                </tr>
                                <tr>
                                    <td>Nodes</td>
                                    <td>:</td>
                                    <td>{{ $data['node']->num_of_node }}</td>
                                </tr>
                                <tr>
                                    <td>Worth</td>
                                    <td>:</td>
                                    <td>${{ number_format($data['node']->needed * $data['node']->price) }}</td>
                                </tr>
                                <tr>
                                    <td>Column1</td>
                                    <td>:</td>
                                    <td>$1</td>
                                </tr>
                                <tr>
                                    <td>Column2</td>
                                    <td>:</td>
                                    <td>$2</td>
                                </tr>
                                <tr>
                                    <td>Column3</td>
                                    <td>:</td>
                                    <td>$3</td>
                                </tr>
                                <tr>
                                    <td>Column4</td>
                                    <td>:</td>
                                    <td>$4</td>
                                </tr>
                                <tr>
                                    <td>Column5</td>
                                    <td>:</td>
                                    <td>$5</td>
                                </tr>
                                <tr>
                                    <td>Column6</td>
                                    <td>:</td>
                                    <td>$6</td>
                                </tr>
                                <tr>
                                    <td>Column7</td>
                                    <td>:</td>
                                    <td>$7</td>
                                </tr>
                                <tr>
                                    <td>Column8</td>
                                    <td>:</td>
                                    <td>$8</td>
                                </tr>
                                <tr>
                                    <td>Column9</td>
                                    <td>:</td>
                                    <td>$9</td>
                                </tr>
                                <tr>
                                    <td>Column10</td>
                                    <td>:</td>
                                    <td>$10</td>
                                </tr>                                
                            </tbody>
                        </table>
                        <!-- <p>Price <span class="tab">: <strong>$0.03</strong></span></p>
                        <p>Reward Frequency <span class="tab">: <strong>6 Days 23 Hours 59 Minutes</strong></span></p> -->

                        <br>
                        <h6>How To Join Masternode</h6>
                        <p>Minimum Contribution <span class="tab">: <strong>50 ACOIN</strong></span></p>
                        <p>Send minimum of 50 ACOIN or more to below address.</p>
                        <div class="col-lg-4 col-centered">
                            <div class="form-group">
                                <div class="input-group">
                                  <input type="text" name="address" value="0xABCDE1134DDFF3424099BDF" id="address" class="form-control" placeholder="Address" aria-label="address" aria-describedby="basic-addon2">
                                  <div class="input-group-append">
                                    <button class="btn btn-outline-secondary btn-round btn-copy" type="button" data-clipboard-target="#address" data-toggle="tooltip" title="Copy to Clipboard">Copy</button>
                                  </div>
                                </div>
                            </div>
                            <p class="text-center"><a href="#">? I need help transfering my coin</a></p>
                        </div>
                        <p>After you made the transfer please check <a href="#">My Balance</a> Menu under "Balance". When you make the transfer please be aware that transaction fee might occur</p>
                        <p>We will credit your balance according to the transferred amount minus the transaction fee.</p>
                        <p>After your balance passed the ninimum amount needed to join masternode, your coin will be put on queue.</p>
                        <p>When a minimum required amount needed for the masternode to run is reached, your queued balance will then moved to active masternode.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/clipboardjs/clipboard.min.js') }}"></script>
    <script type="text/javascript">
            $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            })

            function setTooltip(btn, message) {
            $(btn).tooltip('hide')
                .attr('data-original-title', message)
                .tooltip('show');
            }

            function hideTooltip(btn) {
            setTimeout(function() {
                $(btn).tooltip('hide');
            }, 1000);
            }

            // Clipboard

            var clipboard = new ClipboardJS('button');

            clipboard.on('success', function(e) {
            setTooltip(e.trigger, 'Copied!');
            hideTooltip(e.trigger);
            });

            clipboard.on('error', function(e) {
            setTooltip(e.trigger, 'Failed!');
            hideTooltip(e.trigger);
            });
    </script>
@endpush