@extends('layouts.app')

@push('styles')
    <!-- JQuery DataTable Css -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css') }}">
    <style type="text/css">   
        .col-centered {
            float: none;
            margin: 0 auto;
        }
    </style>
@endpush

@section('content')
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>My Balance
                <small>Welcome to Nodemaster</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Nodemaster</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                    <li class="breadcrumb-item active">My Balance</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body table-responsive">
                        <div class="col-lg-4 col-centered">
                            <h3><strong>My</strong> Balances </h3>
                        </div>
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="dataTable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Balance Available</th>
                                    <th>Balance Pending</th>
                                    <th>Balance Masternode</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Balance Available</th>
                                    <th>Balance Pending</th>
                                    <th>Balance Masternode</th>
                                    <th>Detail</th>
                                </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples --> 
    </div>
</section>
@endsection

@push('scripts')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/bundles/datatablescripts.bundle.js') }}"></script>
    <!-- Custom Js -->
    <script type="text/javascript">
       $(function () { 
            $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('balances.datatable') }}",
                columns: [
                    {data: 'ticker', name: 'ticker'},
                    {data: 'amount_available', name: 'amount_available'},
                    {data: 'amount_lock', name: 'amount_lock'},
                    {data: 'amount_mn', name: 'amount_mn'},
                    {data: 'detail', name: 'detail'}
                ]
            }); 
        });
    </script>
@endpush