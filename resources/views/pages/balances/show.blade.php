@extends('layouts.app')

@push('styles')
    <style type="text/css">    
        .col-centered {
            float: none;
            margin: 0 auto;
        }
    </style>
@endpush 

@section('content')
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Acoin Balance
                <small>Welcome to Nodemaster</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Nodemaster</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                    <li class="breadcrumb-item active">Acoin Balance</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body table-responsive">
                        <div class="col-lg-4 col-centered">
                            <h3><strong>ACOIN</strong> Balance </h3>
                        </div>

                        <h6>Acoin Address</h6>
                         <div class="col-lg-4">
                            <div class="form-group">
                                <div class="input-group">
                                  <input type="text" name="address" value="0xABCDE1134DDFF3424099BDF" id="address" class="form-control" placeholder="Address" aria-label="address" aria-describedby="basic-addon2">
                                  <div class="input-group-append">
                                    <button class="btn btn-outline-secondary btn-round btn-copy" type="button" data-clipboard-target="#address" data-toggle="tooltip" title="Copy to Clipboard">Copy</button>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <br>

                        <h6>Balance Detail</h6>
                        <p>Balance Available <span class="tab">: <strong>70 ACOIN</strong> <button class="btn btn-sm btn-info btn-round">Withdraw</button></span></p>
                        <p>Balance Pending <span class="tab">: <strong>150 ACOIN</strong> <a href="#" data-toggle="tooltip" title="Balance Pending is your balance queued for upcoming masternode">(?)</a> <button class="btn btn-sm btn-info btn-round">Withdraw</button></span></p>
                        <p>Balance in Master Node <span class="tab">: <strong>3000 ACOIN</strong> <button class="btn btn-sm btn-info btn-round">Detail</button></span></p>
                        <br>

                        <h6>History</h6>
                        <div class="body table-responsive">
                        <table class="table">
                            <thead class="bg-info text-white">
                                <tr>
                                    <th>Date</th>
                                    <th>Trx Type</th>
                                    <th>Amount</th>
                                    <th>Additional Info</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">2018-04-12 20:30</th>
                                    <td>MN Payout</td>
                                    <td>70</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">2018-04-08 19:30</th>
                                    <td>Transfer In</td>
                                    <td>70</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">2018-04-12 20:30</th>
                                    <td>Transfer In</td>
                                    <td>70</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row">2018-04-12 20:30</th>
                                    <td>Transfer In</td>
                                    <td>70</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/clipboardjs/clipboard.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        })

        function setTooltip(btn, message) {
            $(btn).tooltip('hide')
            .attr('data-original-title', message)
            .tooltip('show');
        }

        function hideTooltip(btn) {
            setTimeout(function() {
                $(btn).tooltip('hide');
            }, 1000);
        }

        // Clipboard

        var clipboard = new ClipboardJS('button');

        clipboard.on('success', function(e) {
            setTooltip(e.trigger, 'Copied!');
            hideTooltip(e.trigger);
        });

        clipboard.on('error', function(e) {
            setTooltip(e.trigger, 'Failed!');
            hideTooltip(e.trigger);
        });
    </script>
@endpush