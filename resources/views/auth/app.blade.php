<!doctype html>
<html class="no-js " lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Masternode Website">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset('assets/images/favicon.ico') }}" type="image/x-icon">
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('assets/auth/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/auth/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/auth/css/authentication.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/auth/css/color_skins.css') }}">
</head>

<body class="theme-blue authentication sidebar-collapse">
@yield('content')

<!-- Jquery Core Js -->
<script src="{{ asset('assets/auth/bundles/libscripts.bundle.js') }}"></script>
<script src="{{ asset('assets/auth/bundles/vendorscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js -->
</body>
</html>
