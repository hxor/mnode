@extends('auth.app')

@section('content')
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top navbar-transparent">
    <div class="container">        
        <div class="navbar-translate n_logo">
            <a class="navbar-brand" href="javascript:void(0);" title="" target="_blank">Masternode</a>
            <button class="navbar-toggler" type="button">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="navbar-collapse">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/home') }}">Home</a>
                    </li>      
                    <li class="nav-item">
                        <a class="nav-link btn btn-white btn-round" href="{{ route('login') }}">LOGIN</a>
                    </li>
                </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->
<div class="page-header">
    <div class="page-header-image" style="background-image:url({{ asset('assets/images/login.jpg') }})"></div>
    <div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                <form class="form" method="" action="">
                    <div class="header">
                        <div class="logo-container">
                            <img src="{{ asset('assets/images/logo.svg') }}" alt="">
                        </div>
                        <h5>COMPLETE REGISTER</h5>
                        <span>Your registration is now complete, please login to continue</span>
                    </div>
                    <div class="footer text-center">
                        <a href="{{ route('login') }}" class="btn btn-info btn-round btn-lg btn-block waves-effect waves-light">GO TO LOGIN</a>                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
