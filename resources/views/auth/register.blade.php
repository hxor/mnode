@extends('auth.app')

@section('content')
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top navbar-transparent">
    <div class="container">        
        <div class="navbar-translate n_logo">
            <a class="navbar-brand" href="javascript:void(0);" title="" target="_blank">Masternode</a>
            <button class="navbar-toggler" type="button">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="navbar-collapse">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/home') }}">Home</a>
                    </li>      
                    <li class="nav-item">
                        <a class="nav-link btn btn-white btn-round" href="{{ route('login') }}">LOGIN</a>
                    </li>
                </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->
<div class="page-header">
    <div class="page-header-image" style="background-image:url({{ asset('assets/images/login.jpg') }})"></div>
    <div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                <form class="form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="header">
                        <div class="logo-container">
                            <img src="{{ asset('assets/images/logo.svg') }}" alt="">
                        </div>
                        <h5>REGISTER</h5>
                    </div>
                    <div class="content">                                                
                        <div class="form-group{{ $errors->has('username') ? ' has-danger' : ' ' }}">
                            <div class="input-group input-lg">
                                <input type="text" class="form-control" placeholder="Username" name="username" value="{{ old('username') }}" required autofocus>
                                <span class="input-group-addon">
                                    <i class="zmdi zmdi-account-circle"></i>
                                </span>
                            </div>
                            @if ($errors->has('username'))
                                <label id="name-error" class="error text-danger" for="name">{{ $errors->first('username') }}.</label>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : ' ' }}">
                            <div class="input-group input-lg">
                                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required>
                                <span class="input-group-addon">
                                    <i class="zmdi zmdi-email"></i>
                                </span>
                            </div>
                            @if ($errors->has('email'))
                                <label id="name-error" class="error text-danger" for="name">{{ $errors->first('email') }}.</label>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : ' ' }}">
                            <div class="input-group input-lg">
                                <input type="password" placeholder="Password" class="form-control" id="password" name="password" required/>
                                <span class="input-group-addon">
                                    <i class="zmdi zmdi-lock"></i>
                                </span>
                            </div>
                            @if ($errors->has('password'))
                                <label id="name-error" class="error text-danger" for="name">{{ $errors->first('password') }}.</label>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="input-group input-lg">
                                <input type="password" placeholder="Retype Password" class="form-control" id="password_confirmation" name="password_confirmation" required/>
                                <span class="input-group-addon">
                                    <i class="zmdi zmdi-lock"></i>
                                </span>
                            </div>
                        </div>                       
                    </div>
                    <div class="footer text-center">
                        <button type="submit" class="btn btn-info btn-round btn-lg btn-block ">REGISTER</button>                
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
