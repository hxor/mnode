<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
// // Password Reset Routes...
// Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
// Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
// Route::post('password/reset', 'Auth\ResetPasswordController@reset');
// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::get('/success', 'Auth\RegisterController@success')->name('success');

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'nodes', 'as' => 'nodes.'], function () {
    Route::get('/', 'NodesController@index')->name('index');
    Route::get('/{id}/show', 'NodesController@show')->name('show');
    Route::get('/datatable', 'NodesController@dataTable')->name('datatable');
});

Route::group(['prefix' => 'my-nodes', 'as' => 'my.nodes.'], function () {
    Route::get('/', function () {
        return view('pages.my-nodes.index');
    })->name('index');
    Route::get('/detail', function () {
        return view('pages.my-nodes.show');
    })->name('show');
});

Route::group(['prefix' => 'balances', 'as' => 'balances.'], function () {
    Route::get('/', 'BalancesController@index')->name('index');
    Route::get('/{id}/show', 'BalancesController@show')->name('show');
    Route::get('/datatable', 'BalancesController@dataTable')->name('datatable');
});
